package com.hillel.elementary.teamleaders.vitalii.UML;

public class CircleWithCoordinates extends Circle {
    private Point center;

    public CircleWithCoordinates(Point center) {
        this.center = center;
    }

    public CircleWithCoordinates(double radius, Point center) {
        super(radius);
        this.center = center;
    }

    public CircleWithCoordinates(double radius, String color, Point center) {
        super(radius, color);
        this.center = center;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
