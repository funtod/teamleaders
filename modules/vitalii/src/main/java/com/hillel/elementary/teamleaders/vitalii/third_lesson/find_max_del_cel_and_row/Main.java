package com.hillel.elementary.teamleaders.vitalii.third_lesson.find_max_del_cel_and_row;

public class Main {
    public static void main(String[] args) {
        Matrix myMatrix = new Matrix();

        myMatrix.setMatrixSize();
        myMatrix.createMatrix();
        myMatrix.searchMaxElements();
        myMatrix.printMatrix();
        myMatrix.deleteRowAndColsWithMaxEl();
        myMatrix.printMatrix();


        //for test use this code
/*
        myMatrix.test1();
        myMatrix.printMatrix();
        System.out.println("Max element in matrix == " + myMatrix.searchMaxElements() + "\n");
        //System.out.println(myMatrix.getMaxNumRows());
        //System.out.println(myMatrix.getMaxNumCols());
        myMatrix.deleteRowAndColsWithMaxEl();
        myMatrix.printMatrix();*/
    }
}
