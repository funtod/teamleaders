package com.hillel.elementary.teamleaders.vitalii.third_lesson.find_max_del_cel_and_row;

import java.util.Arrays;
import java.util.Scanner;

/*
* Workflow:
* 1. get size of future matrix
* 2. create matrix with inputed size
* 3. Find maximum element in matrix
* 4. Find rows and cels where max element is, and build a map with these coordinates.
* 5. Calculate, how many rows and cels will leave, after "deletion" containing maximum element.
* 6. If quantity is equal zero - finish work. If more than zero, than create new two-dimensional array,
* and copy cels and rows form old matrix.
* */

public class Matrix {
    private int size = 0;
    private int maxNumber;
    private int[][] matrix;
    private int[] rowsToPass;   //number of row, which should be passed (not copied to new array)
    private int[] colsToPass;
    private boolean sizeSet = false;

    public void setMatrixSize() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please, enter matrix size (integer number): ");

        while ( !sizeSet ) {
            if ( scanner.hasNextInt() ){
                size = scanner.nextInt();

                //check inputed number. If zero == exit. If negative == invert to positive.
                if ( size == 0 ){
                    System.out.println("Size of matrix you have entered, is Zero. End of work.");

                    System.exit(-1);
                } else if ( size < 0 ) {
                    size = -size;
                }

                sizeSet = true;
            } else {
                System.out.println("Entered symbols are NOT integer number. Please, try again.");

                //throw away trash from input stream
                String tmp = scanner.nextLine();
                scanner.reset();
            }
        }
    }

    public void createMatrix(){
        matrix = new int[size][size];

        fillMatrix(matrix);
    }

    private void fillMatrix(int[][] matrix){
        int maxNumber = size * 2 + 1;

        for (int row = 0; row < matrix.length; ++row ) {
            for ( int col = 0; col < matrix[row].length; col ++ ) {
                matrix[row][col] = (int)(Math.random() * maxNumber) - size;
            }
        }
    }

    public int searchMaxElements(){
        rowsToPass = new int[size];
        colsToPass = new int[size];
        int max = -size;

        //find biggest number in matrix
        for (int row = 0; row < size; ++row ) {
            for ( int col = 0; col < size; col ++ ) {
                if ( matrix[row][col] > max ) {
                    max = matrix[row][col];
                }
            }
        }
        maxNumber = max;

        //find positions of biggest numbers in matrix
        for (int row = 0; row < size; ++row ) {
            for ( int col = 0; col < size; col ++ ) {
                if ( matrix[row][col] == max ) {
                    rowsToPass[row] = max;
                    colsToPass[col] = max;
                }
            }
        }

        return max;
    }

    private int[][] calcNewMatrixSize() {
        int rowNewNum = 0;
        int colNewNum = 0;

        //calc new matrix size. If row or col equal of max number, than this row and all cols should be throw away.
        for (int i = 0; i < size; ++i ) {
            if ( rowsToPass[i] != maxNumber ){
                rowNewNum += 1;
            }

            if ( colsToPass[i] != maxNumber ){
                colNewNum += 1;
            }
        }

        //check for zero quantity of rows and cols!
        if ( rowNewNum == 0 || colNewNum == 0 ) {
            System.out.println("Quantity of rows or cols in new matrix is equal zero. Nothing to show. Finish application.");

            System.exit(-1);
        }

        int[][] newMatrix = new int[rowNewNum][colNewNum];

        return newMatrix;
    }

    public void deleteRowAndColsWithMaxEl(){
        int[][] newMatrix = calcNewMatrixSize();

        for (int rowOld = 0, rowNew = 0; rowOld < matrix.length && rowNew < newMatrix.length; ++rowOld ) {
            if ( rowsToPass[rowOld] == maxNumber ) {
                continue;
            } else {
                for ( int colOld = 0, colNew = 0; colOld < matrix[rowOld].length && colNew < newMatrix[rowNew].length; colOld ++ ) {
                    if ( colsToPass[colOld] == maxNumber ) {
                        continue;
                    } else {
                        newMatrix[rowNew][colNew] = matrix[rowOld][colOld];
                        colNew += 1;
                    }
                }

                rowNew += 1;
            }
        }

        matrix = newMatrix;
        //ToDo: Set new size for new matrix, if task will be used in loop
    }

    public void printMatrix(){
        for( int row = 0; row < matrix.length; ++row ){
            int bfrLastCol = matrix[row].length - 1;

            System.out.print("[");
            for ( int col = 0; col < bfrLastCol; ++col ){
                //System.out.print("["); //print every number in []

                if ( matrix[row][col] < 0 ) {
                    //System.out.print(matrix[row][col] + "], ");
                    System.out.print(matrix[row][col] + ", ");
                } else {
                    //System.out.print(" " + matrix[row][col] + "], ");
                    System.out.print(" " + matrix[row][col] + ", ");
                }
            }

            if ( matrix[row][bfrLastCol] < 0 ) {
                //System.out.println("[" + matrix[row][bfrLastCol] + "]]");
                System.out.println(matrix[row][bfrLastCol] + "]");
            } else {
                //System.out.println("[ " + matrix[row][bfrLastCol] + "]]");
                System.out.println(" " + matrix[row][bfrLastCol] + "]");
            }
        }
        System.out.println();
    }

    public String getMaxNumRows(){
        return Arrays.toString(rowsToPass);
    }

    public String getMaxNumCols(){
        return Arrays.toString(colsToPass);
    }

    public void test1(){
        size = 5;
        int[][] tmp = {{-1, 0, 0, 0, 5},{4, 4, 0, 2, -5},{1, 1, 3, 5, 4},{3, 1, -3, 0, 0},{-1, -2, 4, 0, 2}};
        matrix = tmp;
    }

    public void test2(){
        size = 6;
        int[][] tmp = {{-6, 6, 3, -2, 6, -4}, {-1, 1, 3, -3, -5, -5}, { -6, -1, -2, 6, 3, 6}, {-6, 5, 1, 1, 5, -6},{ 2, 3, -6, -5, 5, 1}, { 5, -2, 0, 0, -2, -3}};
        matrix = tmp;
    }

    public void test3(){
        size = 2;
        int[][] tmp = {{2, 2},{1, -1}};
        matrix = tmp;
    }
}