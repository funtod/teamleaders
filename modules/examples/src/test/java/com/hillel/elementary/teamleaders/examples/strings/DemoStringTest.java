package com.hillel.elementary.teamleaders.examples.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class DemoStringTest {

    @Test
    void shouldReturnArrayOfWords() {
        String[] expectedResult = {"JAVA", "SE", "8"};

        char[] arrayOfChars = {'J', 'a', 'v', 'a'};

        String[] result = DemoString.doDemoString(arrayOfChars);

        assertArrayEquals(expectedResult, result);
    }

    @Test
    void shouldReturnEmptyArrayIfEmptyArrayOfCharsPassed() {
        String[] expectedResult = {};

        char[] arrayOfChars = {};

        String[] result = DemoString.doDemoString(arrayOfChars);

        assertArrayEquals(expectedResult, result);
    }
}