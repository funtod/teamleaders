package com.hillel.elementary.teamleaders.examples.arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    @Test
    void shouldFindIndex() {
        int[] array = new int[] {1, 2, 4, 10, 22, 34, 66, 1224, 53453};

        int result = BinarySearch.findIndex(10, array);

        assertEquals(3, result);
    }

    @Test
    void shouldReturnMinusOneIfNotFound() {
        int[] array = new int[] {1, 2, 4, 10, 22, 34, 66, 1224, 53453};

        int result = BinarySearch.findIndex(11, array);

        assertEquals(-1, result);
    }

}