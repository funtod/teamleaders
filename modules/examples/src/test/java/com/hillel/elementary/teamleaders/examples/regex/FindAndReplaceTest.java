package com.hillel.elementary.teamleaders.examples.regex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindAndReplaceTest {

    @Test
    void shouldReplaceAllJacks() {
        String expectedText = "This is a story about Tory the barber. He was a good man .... But than Tory bought a hatchet";
        String text = "This is a story about Jack the barber. He was a good man .... But than Jack bought a hatchet";

        String result = FindAndReplace.findAndReplace("Jack", "Tory", text);

        assertEquals(expectedText, result);
    }


    @Test
    void shouldReplaceEquation() {
        String expectedText = "This is four-three-two-One text";
        String text = "This is One:two:three:four text";

        String result = FindAndReplace.findAndReplace("(\\w+):(\\w+):(\\w+):(\\w+)", "$4-$3-$2-$1", text);

        assertEquals(expectedText, result);
    }

    @Test
    void shouldAddBlackAfterJacks() {
        String expectedText = "This is a story about Jack Black the barber. He was a good man .... But than Jack Black bought a hatchet";
        String text = "This is a story about Jack the barber. He was a good man .... But than Jack bought a hatchet";

        String result = FindAndReplace.findAndAppendAfter("Jack", " Black", text);

        assertEquals(expectedText, result);
    }
}