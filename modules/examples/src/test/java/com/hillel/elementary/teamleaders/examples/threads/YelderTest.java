package com.hillel.elementary.teamleaders.examples.threads;

import org.junit.jupiter.api.Test;


class YelderTest {

    @Test
    void shouldYeld() throws InterruptedException {
        int numberOfCycles = 5;
        Yelder yelder = new Yelder(numberOfCycles);
        yelder.setPriority(Thread.MAX_PRIORITY);
        yelder.setName("Yelder");
        yelder.start();

        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        for (int i = 0; i < numberOfCycles; i++) {
            Thread.yield();
            System.out.println(Thread.currentThread().getName() + " in control");
        }
    }
}