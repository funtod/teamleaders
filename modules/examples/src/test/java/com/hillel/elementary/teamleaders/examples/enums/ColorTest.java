package com.hillel.elementary.teamleaders.examples.enums;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ColorTest {

    @Test
    void shouldReturnRedColorByHex() {
        Color color = Color.RED;
        assertEquals(color, Color.fromHex("#dd2c2c"));
    }

    @Test
    void shouldReturnGreenColorByHex() {
        Color color = Color.BLUE;
        assertEquals(color, Color.fromHex("#4290ae"));
    }

    @Test
    void shouldReturnBlueColorByHex() {
        Color color = Color.GREEN;
        assertEquals(color, Color.fromHex("#23b75f"));
    }

    @Test
    void shouldReturnWhiteColorByHex() {
        Color color = Color.WHITE;
        assertEquals(color, Color.fromHex("#fafafa"));
    }

    @Test
    void shouldReturnBlackColorByHex() {
        Color color = Color.BLACK;
        assertEquals(color, Color.fromHex("#000000"));
    }

    @Test
    void shouldReturnGrayColorByHex() {
        Color color = Color.GRAY;
        assertEquals(color, Color.fromHex("#999999"));
    }

    @Test
    void shouldReturnYellowColorByHex() {
        Color color = Color.YELLOW;
        assertEquals(color, Color.fromHex("#ffc200"));
    }

    @Test
    void shouldThrowExceptionIfNoColorFound() {
        assertThrows(IllegalArgumentException.class, () -> Color.fromHex("#2aannzz"));
    }
}