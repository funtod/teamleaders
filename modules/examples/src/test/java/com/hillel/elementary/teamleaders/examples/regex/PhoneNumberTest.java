package com.hillel.elementary.teamleaders.examples.regex;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class PhoneNumberTest {

    @Test
    void shouldFindPhoneNumbers() {
        String[] expectedResult = {"1233323322", "555-55-55 25"};
        String text = "My phone number is 1233323322. Don't forget it. And this is my second one 555-55-55 25";

        List<String> result = PhoneNumber.getPhoneNumbers(text);

        assertArrayEquals(expectedResult, result.toArray());
    }

    @Test
    void shouldFindPhoneNumbersWithCountryCode() {
        String[] expectedResult = {"+38 (066) 55-33-55"};
        String text = "Call +38 (066) 55-33-55 and get free IPhone now!";

        List<String> result = PhoneNumber.getPhoneNumbers(text);

        assertArrayEquals(expectedResult, result.toArray());
    }
}