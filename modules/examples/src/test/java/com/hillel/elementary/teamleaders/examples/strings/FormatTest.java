package com.hillel.elementary.teamleaders.examples.strings;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FormatTest {

    @Test
    void shouldFormatAString() {
        String result = Format.format(2.9112f, 622, "Rabbit");

        assertEquals("The value of the float variable is 2.91, while " +
                "the value of the integer variable is 622, " +
                "and the string is Rabbit", result);
    }

    @Test
    void shouldFormatDate() {
        LocalDate date = LocalDate.parse("2018-05-28");
        // tA - день недели, tB - месяц, te - дата, tY - год
        String result = String.format("Today is %1$tA %1$tB %1$te %1$tY", date);

        assertEquals("Today is Monday May 28 2018", result);
    }

    @Test
    void shouldUserArgumentIndex() {
        String result = String.format("%2$s, the number is %1$d", 32, "Hello");

        assertEquals("Hello, the number is 32", result);
    }

    @Test
    void shouldFormatDigits() {
        assertEquals("|10,000,000|", String.format("|%,d|", 10000000)); //разделитель тысяч

        assertEquals("|(36)|", String.format("|%(d|", -36)); // негативные цифры в скобках

        assertEquals("|                 +93|", String.format("|%+20d|", 93)); // положительные значения с +

        assertEquals("|0000000093|", String.format("|%010d|", 93)); // 10 знаков с нулями спереди

        assertEquals("|135|", String.format("|%o|", 93)); // в восмеричной системе
        assertEquals("|0135|", String.format("|%#o|", 93)); // в восмеричной системе с ведущим нулем

        assertEquals("|5d|", String.format("|%x|", 93)); // хекс
        assertEquals("|0x5d|", String.format("|%#x|", 93)); // хекс с ведущим 0х
        assertEquals("|0X5D|", String.format("|%#X|", 93)); // хекс с ведущим 0Х
    }

    @Test
    void shouldFormatStrings() {
        assertEquals("|Hello World|", String.format("|%s|", "Hello World"));

        assertEquals("|                   Hello World|", String.format("|%30s|", "Hello World")); // указываем длину поля

        assertEquals("|Hello World                   |", String.format("|%-30s|", "Hello World")); // выравнивание слева

        assertEquals("|Hello|", String.format("|%.5s|", "Hello World")); // задаем максимальное количество символов
    }
}