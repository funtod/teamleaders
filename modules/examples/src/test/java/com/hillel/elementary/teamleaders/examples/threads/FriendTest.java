package com.hillel.elementary.teamleaders.examples.threads;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class FriendTest {

    @Test
    @Disabled
    void shouldDeadLock() throws InterruptedException {
        Friend jareth = new Friend("jareth");
        Friend cory = new Friend("cory");

        jareth.becomeFriend(cory);
        cory.becomeFriend(jareth);

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1000; i++)
                    jareth.hug();
            }
        }, "thread1");

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1000; i++)
                    cory.hug();
            }
        }, "thread2");

        t1.start();
        t2.start();

        t1.join();
        t2.join();
        t1.interrupt();
    }
}