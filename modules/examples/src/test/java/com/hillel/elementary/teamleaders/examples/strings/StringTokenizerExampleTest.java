package com.hillel.elementary.teamleaders.examples.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringTokenizerExampleTest {

    @Test
    void shouldParseStringWithProducts() {
        Product[] expectedResult = {
                new Product(1, 2.0, "Vanya"),
                new Product(2, 2.1, "Jack"),
                new Product(3, 0.2, "Tamil"),
                new Product(4, 2.5, "Leeroy"),
        };
        String text = "1|2.0|Vanya\n" +
                "2|2.1|Jack\n" +
                "3|0.2|Tamil\n" +
                "4|2.5|Leeroy";

        Product[] products = StringTokenizerExample.parseProducts(text);

        assertArrayEquals(expectedResult, products);

    }
}