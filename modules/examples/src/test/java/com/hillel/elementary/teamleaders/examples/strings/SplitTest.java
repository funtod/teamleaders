package com.hillel.elementary.teamleaders.examples.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SplitTest {

    @Test
    void shouldTrimAndSplit() {
        String[] expectedResult = {"albina", "Alena", "Alice", "alina", "ALLA", "Anastasya", "ArinA"};

        String[] result = Split.getSortedNames(" Alena Alice alina albina Anastasya ALLA ArinA ");

        assertArrayEquals(expectedResult, result);
    }
}