package com.hillel.elementary.teamleaders.examples.arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InsertionSortTest {

    @Test
    void shouldSortAnArrayOfOne() {
        int[] arrayToSort = new int[]{0};
        int[] expectedArray = new int[]{0};

        InsertionSort.sort(arrayToSort);

        assertArrayEquals(expectedArray, arrayToSort);
    }

    @Test
    void shouldSortArrayWithDuplicates() {
        int[] arrayToSort = new int[]{0, 1, 0, 1};
        int[] expectedArray = new int[]{0, 0, 1, 1};

        InsertionSort.sort(arrayToSort);

        boolean b1 = Boolean.FALSE;

        assertArrayEquals(expectedArray, arrayToSort);
    }

    @Test
    void shouldSortArray() {
        int[] arrayToSort = new int[]{1, 4, 6, 1, 7, 0, 20, 22, 442, 4, 1, 5};
        int[] expectedArray = new int[]{0, 1, 1, 1, 4, 4, 5, 6, 7, 20, 22, 442};

        InsertionSort.sort(arrayToSort);

        assertArrayEquals(expectedArray, arrayToSort);
    }

}