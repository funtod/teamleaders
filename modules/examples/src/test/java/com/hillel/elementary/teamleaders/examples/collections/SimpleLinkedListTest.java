package com.hillel.elementary.teamleaders.examples.collections;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SimpleLinkedListTest {


    @Test
    void shouldRemoveItem() {
        SimpleLinkedList list = new SimpleLinkedList();

        Point point = new Point(1, 2);
        list.add(point);

        boolean removed = list.remove(new Point(1, 2));

        assertThat(removed).isTrue();
        assertThat(list.size()).isEqualTo(0);
    }
}