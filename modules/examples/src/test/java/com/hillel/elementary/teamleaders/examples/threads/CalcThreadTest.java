package com.hillel.elementary.teamleaders.examples.threads;

import org.junit.jupiter.api.Test;

class CalcThreadTest {

    @Test
    void shouldJoin() {
        CalcThread calc = new CalcThread();
        calc.start();

        try {
            calc.join();
            System.out.println("The result is " + calc.getResult());
        } catch (InterruptedException e) {
            System.out.println("the thread is interrupted");
        }
    }
}