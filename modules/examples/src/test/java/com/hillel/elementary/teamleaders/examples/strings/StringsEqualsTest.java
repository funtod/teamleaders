package com.hillel.elementary.teamleaders.examples.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringsEqualsTest {

    @Test
    void shouldCheckStringEquality() {
        String s1 = "Java";
        String s2 = "Java";
        String s3 = new String("Java");
        String s4 = new String(s1);

        assertTrue(s1 == s2); // true

        assertFalse(s3 == s4); // false

        assertFalse(s1 == s3); // false

        assertTrue(s1.equals(s2)); // true

        assertTrue(s1.equals(s3)); // true

        s3 = s3.intern(); // переопределяем ссылку s3 на объект из пула
        assertTrue(s1 == s3); // true


    }
}
