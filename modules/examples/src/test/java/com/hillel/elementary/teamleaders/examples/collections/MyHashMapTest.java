package com.hillel.elementary.teamleaders.examples.collections;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MyHashMapTest {

    @Test
    void shouldAddAndGetElementsFromMap() {
        Map<Point, String> map = new MyHashMap<>();

        map.put(new Point(1, 3), "One Three");
        map.put(new Point(1, 1), "One One");
        map.put(new Point(1, 1), "One One new");
        map.put(new Point(4, 1), "Four One");


        assertThat(map.get(new Point(1, 3))).isEqualTo("One Three");
        assertThat(map.get(new Point(1, 1))).isEqualTo("One One new");
        assertThat(map.get(new Point(4, 1))).isEqualTo("Four One");
        assertThat(map.size()).isEqualTo(3);
    }

    @Test
    void shouldRemoveElementsFromMap() {
        Map<Point, String> map = new MyHashMap<>();

        map.put(new Point(1, 3), "One Three");
        map.put(new Point(1, 1), "One One");
        map.put(new Point(1, 1), "One One new");


        assertThat(map.remove(new Point(1, 3))).isEqualTo("One Three");
        assertThat(map.remove(new Point(1, 3))).isEqualTo(null);
        assertThat(map.size()).isEqualTo(1);
    }

}