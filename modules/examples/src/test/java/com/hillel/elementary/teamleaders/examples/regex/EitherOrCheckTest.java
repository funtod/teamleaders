package com.hillel.elementary.teamleaders.examples.regex;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class EitherOrCheckTest {

    @Test
    void shouldFindAllJimsAndJoes() {
        String[] expectedResult = {"JIM", "JOE"};
        List<String> result = EitherOrCheck.matchJimOrJoe("humbapumpa jim\r\n hampa dampa\r\n some more text jOe");

        assertArrayEquals(expectedResult, result.toArray());
    }

    @Test
    void shouldFindAllNonJimsAndJoesLines() {
        String[] expectedResult = {"hampa dampa"};
        List<String> result = EitherOrCheck.matchNotAJimJoeLine("humbapumpa jim\r\n hampa dampa\r\n some more text jOe");

        assertArrayEquals(expectedResult, result.toArray());
    }
}