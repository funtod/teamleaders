package com.hillel.elementary.teamleaders.examples.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.assertj.core.api.Assertions.assertThat;

class BufferedReaderExampleTest {

    @Test
    void shouldRunBufferedReader() {
        byte[] input = "abcdeq\nand a full line here\nanother full line\nq".getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        BufferedReaderExample.run(inputStream, outputStream);

        assertThat("PrintWriter puts: Привет, мир\n" +
                "Посимвольный ввод:\na\nb\nc\nd\ne\n" +
                "Построчный ввод:\n\n" +
                "and a full line here\n" +
                "another full line\n").isEqualToIgnoringNewLines(outputStream.toString());
    }
}