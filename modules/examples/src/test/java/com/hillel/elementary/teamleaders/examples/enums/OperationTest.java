package com.hillel.elementary.teamleaders.examples.enums;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperationTest {

    @Test
    void shouldDivide() {
        assertEquals(4, Operation.DIVIDED_BY.eval(8,2));
    }

    @Test
    void shouldSubtract() {
        assertEquals(6, Operation.MINUS.eval(8,2));
    }

    @Test
    void shouldAdd() {
        assertEquals(10, Operation.PLUS.eval(8,2));
    }

    @Test
    void shouldMultiply() {
        assertEquals(16, Operation.TIMES.eval(8,2));
    }

}