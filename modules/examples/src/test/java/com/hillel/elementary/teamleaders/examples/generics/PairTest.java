package com.hillel.elementary.teamleaders.examples.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairTest {

    @Test
    void shouldThrowClassCastException() {
        Pair pair = new Pair(1, 2);

        assertThrows(ClassCastException.class, () -> {
            String string = (String) pair.getFirst();
        });
    }

}