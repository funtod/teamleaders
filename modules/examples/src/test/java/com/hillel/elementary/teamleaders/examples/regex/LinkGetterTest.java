package com.hillel.elementary.teamleaders.examples.regex;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class LinkGetterTest {

    @Test
    void shouldExtractLink() {
        String[] expectedResult = {"https://bit-bucket.org/funtod/teamleaders/pull-requests/11"};
        String text = "<div class=\"title-and-target-branch\">" +
                "<a class=\"pull-request-title\" title=\"Matrixsorting\" " +
                "href=\"https://bit-bucket.org/funtod/teamleaders/pull-requests/11\">" +
                "Matrixsorting</a><span class=\"aui-icon aui-icon-small aui-iconfont-devtools-arrow-right\">" +
                "</span><span class=\"pull-request-target-branch\"><span class=\"ref-label\">" +
                "<span class=\"ref branch\"><span class=\"name\" aria-label=\"branch master\">" +
                "master</span></span></span></span></div>";

        List<String> result = LinkGetter.extractLink(text);

        assertArrayEquals(expectedResult, result.toArray());
    }
}