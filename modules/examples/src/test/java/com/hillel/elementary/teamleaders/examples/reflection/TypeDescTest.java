package com.hillel.elementary.teamleaders.examples.reflection;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static com.hillel.elementary.teamleaders.examples.reflection.TypeDesc.BASIC_TYPE_LABELS;
import static org.assertj.core.api.Assertions.assertThat;

class TypeDescTest {

    @Test
    void shouldReturnAListOfAncestors() {
        TypeDesc desc = new TypeDesc();
        String result = desc.printType(HashMap.class, 0, BASIC_TYPE_LABELS);

        System.out.println(result);
    }
}