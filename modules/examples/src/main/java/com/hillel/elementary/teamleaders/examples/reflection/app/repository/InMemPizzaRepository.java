package com.hillel.elementary.teamleaders.examples.reflection.app.repository;

import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Component;
import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.PostCreate;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Pizza;

import java.util.HashMap;
import java.util.Map;

@Component("pizzaRepository")
public class InMemPizzaRepository implements PizzaRepository {

    private final Map<Long, Pizza> pizzas = new HashMap<>();

    @PostCreate
    private void init() {
        pizzas.put(1L, new Pizza(1, "sea", Pizza.PizzaType.SEA));
        pizzas.put(2L, new Pizza(2, "meat", Pizza.PizzaType.MEAT));
        pizzas.put(3L, new Pizza(3, "vega", Pizza.PizzaType.VEGETARIAN));
    }

    @Override
    public Pizza find(Long id) {
        return pizzas.get(id);
    }
}
