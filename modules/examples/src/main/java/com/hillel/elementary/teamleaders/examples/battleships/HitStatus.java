package com.hillel.elementary.teamleaders.examples.battleships;

public enum HitStatus {
    MISSED,
    HIT,
    SINKED
}
