package com.hillel.elementary.teamleaders.examples.reflection.app.services;

import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Component;
import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Timed;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Customer;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Order;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Pizza;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;

@Component("orderService")
public class SimpleOrderService implements OrderService {

    private final OrderRepository orderRepository;
    private final PizzaService pizzaService;

    public SimpleOrderService(OrderRepository orderRepository, PizzaService pizzaService) {
        this.orderRepository = orderRepository;
        this.pizzaService = pizzaService;
    }

    @Override
    @Timed
    public Order placeNewOrder(Customer customer, Long... pizzaID) {
        List<Pizza> pizzas = new ArrayList<>();

        for (Long id : pizzaID) {
            Pizza pizza = findPizzaByID(id);
            if (pizza == null) throw new IllegalArgumentException(String.format("Pizza with id %d not found", id));
            pizzas.add(pizza);  // get Pizza from predifined in-memory list
        }
        Order newOrder = new Order(customer, pizzas);

        saveOrder(newOrder);  // set Order Id and save Order to in-memory list
        return newOrder;

    }

    private Pizza findPizzaByID(Long id) {
        return pizzaService.find(id);
    }

    private void saveOrder(Order newOrder) {
        orderRepository.save(newOrder);
    }
}
