package com.hillel.elementary.teamleaders.examples.patterns.protorype;

public class Horseman extends Warrior {
    private String horseName;

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    @Override
    public Copyable copy() {
        Horseman copy = new Horseman();
        copy.setHorseName(this.horseName);
        copy.setName(this.getName());
        return copy;
    }

    @Override
    public String toString() {
        return "Horseman{" +
                "horseName='" + horseName + '\'' +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }
}
