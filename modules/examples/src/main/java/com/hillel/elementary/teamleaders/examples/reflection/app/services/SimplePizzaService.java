package com.hillel.elementary.teamleaders.examples.reflection.app.services;

import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Component;
import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Timed;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Pizza;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.PizzaRepository;

@Component("pizzaService")
public class SimplePizzaService implements PizzaService  {

    private PizzaRepository pizzaRepository;

    public SimplePizzaService(PizzaRepository pizzaRepository) {
        this.pizzaRepository = pizzaRepository;
    }

    @Override
    @Timed(isOn = false)
    public Pizza find(Long id) {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return pizzaRepository.find(id);
    }

}
