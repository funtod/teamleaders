package com.hillel.elementary.teamleaders.examples.battleships;

import java.util.Random;

public class PcPlayer extends Player {

    private Random random = new Random();

    public PcPlayer(String name, Field field) {
        super(name, field);
    }

    @Override
    int[] move() {
        int x = random.nextInt(9);
        int y = random.nextInt(9);

        return new int[]{x, y};
    }
}
