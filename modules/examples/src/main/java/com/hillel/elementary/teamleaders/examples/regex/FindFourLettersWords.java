package com.hillel.elementary.teamleaders.examples.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindFourLettersWords {


    public static List<String> find(String text) {
        List<String> words = new ArrayList<>();

        Pattern pattern = Pattern.compile("(^|\\p{Punct}|\\s)+([a-zA-Z]{4})($|\\p{Punct}|\\s)+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            words.add(matcher.group(2).trim());
        }


        return words;
    }
}
