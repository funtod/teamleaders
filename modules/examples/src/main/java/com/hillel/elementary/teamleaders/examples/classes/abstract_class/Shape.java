package com.hillel.elementary.teamleaders.examples.classes.abstract_class;

public abstract class Shape {

    abstract double getArea();
}
