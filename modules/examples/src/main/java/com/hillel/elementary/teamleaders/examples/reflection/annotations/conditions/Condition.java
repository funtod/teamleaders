package com.hillel.elementary.teamleaders.examples.reflection.annotations.conditions;

public enum Condition {
    TRUE,
    FALSE
}
