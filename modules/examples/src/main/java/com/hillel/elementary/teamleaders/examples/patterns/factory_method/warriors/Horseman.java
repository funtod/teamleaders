package com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors;

public class Horseman extends Warrior {
    @Override
    protected String info() {
        return "Horseman";
    }
}
