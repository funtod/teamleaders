package com.hillel.elementary.teamleaders.examples.classes;


public class Overloads {

    public static String viewNum(Integer i) { // 1
        return String.format("Primitives=%d", i);
    }
    public static String viewNum(int i) { // 2
        return String.format("int=%d", i);
    }
    public static String viewNum(Float f) { // 3
        return String.format("Float=%.2f", f);
    }
    public static String viewNum(Number n) { // 4
        return String.format("Number=" + n);
    }

    public static void main(String[ ] args) {
        Number[ ] num = {new Integer(7), 71, 3.14f, 7.2 };
        for (Number n : num) {
            viewNum(n);
        }
        viewNum(new Integer(8));
        viewNum(81);
        viewNum(4.14f);
        viewNum(8.2);
    }
}
