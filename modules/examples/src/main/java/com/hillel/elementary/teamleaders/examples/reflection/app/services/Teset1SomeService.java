package com.hillel.elementary.teamleaders.examples.reflection.app.services;

public class Teset1SomeService implements SomeService {

    @Override
    public String getString() {
        return "Test1";
    }

    public void destroy(){
        System.out.println("Destroy");
    }
}
