package com.hillel.elementary.teamleaders.examples.reflection.app.configuration;

import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

public class ApplicationContext implements Context {

    private final Config config;
    private final Map<String, Object> beans = new HashMap<>();

    public ApplicationContext(Config config) {
        this.config = config;
    }

    @Override
    public <T> T getBean(String beanName) throws Exception {
        Object bean = beans.get(beanName);

        if (bean != null) {
            return (T) bean;
        }

        Class<?> type = config.getImplementation(beanName);

        BeanBuilder builder = new BeanBuilder(type);
        builder.createBean();
        builder.callPostCreateMethod();
        bean = builder.build();

        beans.put(beanName, bean);

        return (T) bean;
    }

    class BeanBuilder {
        private final Class<?> type;
        private Object bean;

        BeanBuilder(Class<?> type) {
            this.type = type;
        }

        void createBean() throws Exception {
            Constructor<?> constructor = type.getConstructors()[0];

            if (constructor.getParameterCount() == 0) {
                bean = type.newInstance();
            } else {
                bean = newInstanceWithParameters(constructor);
            }
        }

        private Object newInstanceWithParameters(Constructor<?> constructor) throws Exception {
            Parameter[] parameters = constructor.getParameters();

            Object[] paramsVal = new Object[parameters.length];

            for (int i = 0; i < parameters.length; i++) {
                Class<?> paramType = parameters[i].getType();
                String beanName = getBeanNameByType(paramType);
                paramsVal[i] = getBean(beanName);
            }

            return constructor.newInstance(paramsVal);
        }

        private String getBeanNameByType(Class<?> paramType) {
            System.out.println("Getting Bean: " + paramType);
            String paramTypeName = paramType.getSimpleName();
            return Character.toLowerCase(paramTypeName.charAt(0)) + paramTypeName.substring(1);
        }

        void callInitMethod() throws Exception {
            Class<?> clazz = bean.getClass();
            Method method;
            try {
                method = clazz.getMethod("init");
            } catch (NoSuchMethodException ex) {

                return;
            }
            method.invoke(bean);
        }

        private void callPostCreateMethod() {
            for (Method method : type.getDeclaredMethods()) {
                if (method.getAnnotation(PostCreate.class) != null) {
                    method.setAccessible(true);
                    try {
                        method.invoke(bean);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        System.out.printf("Error while calling @PostCreate method %s on bean %s", method.getName(), type.getSimpleName());
                    }
                }
            }
        }

        public Object build() {
            return Proxy.newProxyInstance(
                    type.getClassLoader(),
                    type.getInterfaces(),
                    new TimedInvocationHandler(bean));
        }
    }
}
