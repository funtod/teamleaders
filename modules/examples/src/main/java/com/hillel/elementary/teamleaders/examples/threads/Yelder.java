package com.hillel.elementary.teamleaders.examples.threads;

class Yelder extends Thread {
    private int howOften;

    public Yelder(int howOften) {
        this.howOften = howOften;
    }

    public void run() {
        for (int i = 0; i < howOften; i++) {
            System.out.println(Thread.currentThread().getName() + " in control");
        }
    }
}
