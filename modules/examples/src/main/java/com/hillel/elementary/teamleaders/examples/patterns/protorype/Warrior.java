package com.hillel.elementary.teamleaders.examples.patterns.protorype;

public abstract class Warrior implements Copyable {
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
