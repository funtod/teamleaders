package com.hillel.elementary.teamleaders.examples.sockets.chat.client;

import java.io.*;
import java.net.*;

public class WriteThread extends Thread {
    private Socket socket;
    private ChatClient client;

    WriteThread(Socket socket, ChatClient client) {
        this.socket = socket;
        this.client = client;
    }

    public void run() {
        try (PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
             BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            System.out.println("\nEnter your name: ");

            String userName = reader.readLine();
            client.setUserName(userName);

            writer.println(userName);

            String text = readUserInput(reader, userName);
            writer.println(text);

            while (!text.equals("bye")) {
                text = readUserInput(reader, userName);
                writer.println(text);
            }

            socket.close();
        } catch (IOException ex) {
            System.out.println("Error writing to server: " + ex.getMessage());
        }
    }

    private String readUserInput(BufferedReader reader, String userName) throws IOException {
        System.out.println("[" + userName + "]: ");
        return reader.readLine();
    }
}