package com.hillel.elementary.teamleaders.examples.threads;

public class SimpleThread {

    public static void main(String[] args) {

        Thread thread = new Thread();
        thread.setName("This is thread one");
        thread.setPriority(10); //от 1 до 10
        thread.start();

        thread.suspend();
        thread.stop();
    }
}
