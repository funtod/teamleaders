package com.hillel.elementary.teamleaders.examples.threads.tasks;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class WaitTaskWithBarrier {
    Thread t1, t2;
    CyclicBarrier barrier = new CyclicBarrier(2, new BarrierThread("barrier", 3));
    int t1Counter = 0, t2Counter = 0;

    class TestThread implements Runnable {
        String threadName;
        int n;

        public TestThread(String threadName, int n) {
            this.threadName = threadName;
            this.n = n;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                System.out.println(threadName + ":" + i);
                if (n == 1) t1Counter = i;
                if (n == 2) t2Counter = i;
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                Thread.yield();
            }
        }
    }

    class BarrierThread implements Runnable {
        String threadName;
        int n;
        int count = 0;

        public BarrierThread(String threadName, int n) {
            this.threadName = threadName;
            this.n = n;
        }

        @Override
        public void run() {
            if (count % 10 == 0 && count > 0)
                System.out.println(threadName + ":" + count);
            count++;
        }
    }

    public void runJob() {
        t1 = new Thread(new TestThread("t1", 1));
        t2 = new Thread(new TestThread("t2", 2));
        System.out.println("Starting threads");
        t1.start();
        t2.start();

        System.out.println("Waiting for threads");
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new WaitTaskWithBarrier().runJob();
    }
}
