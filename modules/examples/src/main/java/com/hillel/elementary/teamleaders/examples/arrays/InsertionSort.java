package com.hillel.elementary.teamleaders.examples.arrays;

public class InsertionSort {

    public static void sort(int[] array) {

        for (int currentIndex = 1; currentIndex < array.length; currentIndex++) {
            int currentVal = array[currentIndex];
            int leftIndex = currentIndex - 1;

            while (leftIndex >= 0 && currentVal < array[leftIndex]) {
                array[leftIndex + 1] = array[leftIndex--];
            }

            array[leftIndex + 1] = currentVal;
        }
    }
}