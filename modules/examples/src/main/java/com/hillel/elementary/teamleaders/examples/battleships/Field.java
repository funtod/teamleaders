package com.hillel.elementary.teamleaders.examples.battleships;

public class Field {
    private int[][] field = new int[10][10];  // TODO: remove this
    private Cell[][] cells = new Cell[10][10];
    private ShipCreator shipCreator;

    public Field(ShipCreator shipCreator1) {
        this.shipCreator = shipCreator1;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                cells[i][j] = new Cell();
            }
        }
    }

    public void initializeRandomShips() {
        shipCreator.createShip(field, cells, 4);
        shipCreator.createShip(field, cells, 4);
        shipCreator.createShip(field, cells, 3);
        shipCreator.createShip(field, cells, 3);
        shipCreator.createShip(field, cells, 3);
        shipCreator.createShip(field, cells, 2);
        shipCreator.createShip(field, cells, 2);
        shipCreator.createShip(field, cells, 2);
        shipCreator.createShip(field, cells, 2);
        shipCreator.createShip(field, cells, 1);
        shipCreator.createShip(field, cells, 1);
        shipCreator.createShip(field, cells, 1);
        shipCreator.createShip(field, cells, 1);
        shipCreator.createShip(field, cells, 1);
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t1 2 3 4 5 6 7 8 9 10\n");
        for (int i = 0; i < field.length; i++) {
            stringBuilder.append(i + 1).append("\t");
            for (int j = 0; j < field[i].length; j++) {
                stringBuilder.append(field[i][j]  == 0 ?  "~ " : "X ");
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public HitStatus takeHit(int[] coord) {
        Ship ship = cells[coord[0]][coord[1]].getShip();
        if (ship == null) return HitStatus.MISSED;
        else  {
            int livesLeft = ship.hit();
            if (livesLeft <= 0) return HitStatus.SINKED;
            else return HitStatus.HIT;
        }
    }

    public boolean allShipsAreSank() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                Ship ship = cells[i][j].getShip();
                if (ship != null && ship.isAlive()) return false;
            }
        }
        return true;
    }
}
