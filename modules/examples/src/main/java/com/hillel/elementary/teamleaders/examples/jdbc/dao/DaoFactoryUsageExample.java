package com.hillel.elementary.teamleaders.examples.jdbc.dao;

public class DaoFactoryUsageExample {

    public static void main(String[] args) {
        DAOFactory cloudscapeFactory = DAOFactory.getDAOFactory(DAOFactory.FactoryType.CLOUDSCAPE);

        CustomerDAO custDAO = cloudscapeFactory.getCustomerDAO();
        Long newCustId = custDAO.insertCustomer(new Customer());

        //Создаем Customer объект
        Customer customer = custDAO.findCustomer(newCustId);

        //изменяем значение Customer объекта
        customer.setStreetAddress("Klovska str, 22");
        customer.setCity("Kyiv");

        custDAO.updateCustomer(customer);
    }
}
