package com.hillel.elementary.teamleaders.examples.reflection.annotations.users;

public interface Action {
    void invoke(User user);
}
