package com.hillel.elementary.teamleaders.examples.sockets.chat.client;

import java.io.*;
import java.net.*;

public class ReadThread extends Thread {
    private Socket socket;
    private ChatClient client;

    ReadThread(Socket socket, ChatClient client) {
        this.client = client;
        this.socket = socket;
    }

    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {
            while (true) {
                String response = reader.readLine();
                System.out.println("\n" + response);

                // prints the username after displaying the server's message
                if (client.getUserName() != null) {
                    System.out.print("[" + client.getUserName() + "]: ");
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading from server: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}