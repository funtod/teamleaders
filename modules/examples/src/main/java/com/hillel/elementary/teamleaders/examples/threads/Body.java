package com.hillel.elementary.teamleaders.examples.threads;

public class Body {
    public long idNum;
    public String nameFor;
    public Body orbits;

    public static long nextID = 0;

    Body() {
        synchronized (Body.class) {
            idNum = nextID++;
        }
    }
}
