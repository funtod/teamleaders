package com.hillel.elementary.teamleaders.examples.reflection.app.services;

import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Pizza;

public interface PizzaService {

    Pizza find(Long id);
}