package com.hillel.elementary.teamleaders.examples.sockets.chat.client;

import java.net.*;
import java.io.*;

public class ChatClient {
    private String hostname;
    private String userName;

    ChatClient(String hostname) {
        this.hostname = hostname;
    }

    void execute(int port) {
        try(Socket socket = new Socket(hostname, port)) {
            System.out.println("Connected to the chat server");

            ReadThread readThread = new ReadThread(socket, this);
            readThread.start();
            WriteThread writeThread = new WriteThread(socket, this);
            writeThread.start();

            readThread.join();
            writeThread.join();

        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O Error: " + ex.getMessage());
        } catch (InterruptedException ex) {
            System.out.println("Interrupted Exception: " + ex.getMessage());
        }
    }

    void setUserName(String userName) {
        this.userName = userName;
    }

    String getUserName() {
        return this.userName;
    }
}