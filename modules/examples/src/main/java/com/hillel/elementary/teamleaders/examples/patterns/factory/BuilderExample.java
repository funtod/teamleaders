package com.hillel.elementary.teamleaders.examples.patterns.factory;

public class BuilderExample {
    public static void main(String[] args) {
        Director director = new Director();
        ComputerBuilder cheapComputerBuilder = new CheapComputerBuilder();

        director.setComputerBuilder(cheapComputerBuilder);

        Computer computer = director.constructComputer();
    }
}


