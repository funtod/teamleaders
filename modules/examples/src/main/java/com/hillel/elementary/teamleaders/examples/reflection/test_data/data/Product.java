package com.hillel.elementary.teamleaders.examples.reflection.test_data.data;

import java.math.BigDecimal;

public class Product {
    public Long id;
    public String name;
    public BigDecimal price;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
