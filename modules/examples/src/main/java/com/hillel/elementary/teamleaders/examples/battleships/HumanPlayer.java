package com.hillel.elementary.teamleaders.examples.battleships;

import java.util.Scanner;

public class HumanPlayer extends Player {

    public HumanPlayer(String name, Field field) {
        super(name, field);
    }

    @Override
    int[] move() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose a coordinate");
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        if (x < 1 || x > 10 || y < 1 || y > 10) {
            System.out.println("Invalid coordinates");
        }
        return new int[0];
    }
}
