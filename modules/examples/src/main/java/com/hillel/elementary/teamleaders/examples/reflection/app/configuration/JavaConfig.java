package com.hillel.elementary.teamleaders.examples.reflection.app.configuration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class JavaConfig implements Config {

    private Map<String, Class<?>> classes = new HashMap<>();

    public JavaConfig() {
        try {
            ArrayList<Class> allClasses = getAllClasses();
            for (Class aClass : allClasses) {
                Component annotation = (Component) aClass.getAnnotation(Component.class);
                if (annotation != null) {
                    String name = annotation.value().isEmpty() ? aClass.getSimpleName() :  annotation.value();
                    classes.put(name, aClass);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to find Component classes");
        }
    }

    private ArrayList<Class> getAllClasses() throws IOException, ClassNotFoundException {
        Enumeration<URL> resources = getClass().getClassLoader().getResources("");
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, ""));
        }
        return classes;
    }

    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files != null ? files : new File[0]) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName.substring(1).replace("/", ".") + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    @Override
    public Class<?> getImplementation(String name) {
        return classes.get(name);
    }
}
