package com.hillel.elementary.teamleaders.examples.primitives;

public class Ternary {


    public static void main(String[] args) {
        System.out.println(isOdd(3));
    }

    static boolean isOdd(int number) {
        return (number % 2 == 0) ? false : true;
    }
}
