package com.hillel.elementary.teamleaders.examples.patterns.protorype;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PrototypeExample {
    public static void main(String[] args) {

        Archer archer = new Archer();
        archer.setName("Nicola");
        archer.setType(Archer.Type.ONE);

        Horseman horseman = new Horseman();
        horseman.setName("Vasya");
        horseman.setHorseName("Roach");

        List<Warrior> warriors = Arrays.asList(archer, horseman);

        List<Copyable> copyOfWarriors = warriors.stream().map(Copyable::copy).collect(Collectors.toList());

        System.out.println(warriors);
        System.out.println(copyOfWarriors);
    }
}
