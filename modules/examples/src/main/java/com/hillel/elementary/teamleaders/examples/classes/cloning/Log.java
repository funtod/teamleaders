package com.hillel.elementary.teamleaders.examples.classes.cloning;

public class Log {
    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
