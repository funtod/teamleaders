package com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors;

public class Infantryman extends Warrior {

    @Override
    protected String info() {
        return "Infantryman";
    }
}
