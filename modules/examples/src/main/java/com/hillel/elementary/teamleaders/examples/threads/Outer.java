package com.hillel.elementary.teamleaders.examples.threads;

public class Outer {
    private int data;

    private class Inner {
        void setOuterData() {
            synchronized (Outer.this) {
                data = 12;
            }
        }

    }
}
