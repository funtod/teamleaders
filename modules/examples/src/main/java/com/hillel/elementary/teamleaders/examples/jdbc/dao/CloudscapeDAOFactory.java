package com.hillel.elementary.teamleaders.examples.jdbc.dao;

import java.sql.Connection;

public class CloudscapeDAOFactory extends DAOFactory {

    public static final String DRIVER = "COM.cloudscape.core.RmiJdbcDriver";
    public static final String DBURL = "jdbc:cloudscape:rmi://localhost:1099/CoreJ2EEDB";

    public static Connection createConnection() {
        throw new UnsupportedOperationException();
    }

    public CustomerDAO getCustomerDAO() {
        return new CloudscapeCustomerDAO();
    }

//    public AccountDAO getAccountDAO() {
//        return new CloudscapeAccountDAO();
//    }
//
//    public OrderDAO getOrderDAO() {
//        return new CloudscapeOrderDAO();
//    }
}