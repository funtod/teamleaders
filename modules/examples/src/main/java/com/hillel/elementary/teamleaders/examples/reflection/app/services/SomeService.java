package com.hillel.elementary.teamleaders.examples.reflection.app.services;

public interface SomeService {
    public String getString();
}
