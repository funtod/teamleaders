package com.hillel.elementary.teamleaders.examples.threads;

public class PingPong extends Thread {

    String word;
    int delay; // длительность паузы

    PingPong(String whatToSay, int delayTime) {
        word = whatToSay;
        delay = delayTime;
    }

    @Override
    public void run() {
        try {
            for (; ; ) {
                System.out.println(word + " ");
                sleep(delay);
            }
        } catch (InterruptedException e) {
            return;
        }
    }

    public static void main(String[] args) {
        new PingPong("ping", 33).start(); // 1/30 секунды
        new PingPong("PONG", 100).start(); // 1/10 секунды
    }

}