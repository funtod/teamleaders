package com.hillel.elementary.teamleaders.examples.java8.tasks;

enum TaskType {
    READING,
    WRITING,
    SPEAKING
}
