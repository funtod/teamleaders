package com.hillel.elementary.teamleaders.examples.patterns.singleton;

public class LazySingletonWithHolder {

    private LazySingletonWithHolder() {
    }

    public static LazySingletonWithHolder getInstance() {
        return LazySingletonHolder.INSTANCE;
    }

    private static class LazySingletonHolder {
        static final LazySingletonWithHolder INSTANCE = new LazySingletonWithHolder();
    }
}
