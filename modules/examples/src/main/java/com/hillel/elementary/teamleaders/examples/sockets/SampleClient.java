package com.hillel.elementary.teamleaders.examples.sockets;

import java.net.*;

class SampleClient extends Thread {

    public static void main(String args[]) {
        try (// открываем сокет и коннектимся к localhost:3128 получаем сокет сервера
             Socket socket = new Socket("localhost", 3128)) {
            // берём поток вывода и выводим туда HELLO, адрес открытого сокета и его порт
            //Метод getHostAddress() из класса InetAddress возвращает IP хоста в текстовом виде.
            String message = "HELLO " + socket.getInetAddress().getHostAddress() + ":" + socket.getLocalPort();
            socket.getOutputStream().write(message.getBytes());
            // читаем ответ
            byte buf[] = new byte[64 * 1024];
            int read = socket.getInputStream().read(buf);
            String data = new String(buf, 0, read);
            //выводим ответ в консоль
            System.out.println("CLIENT: data received: " + data);
        } catch (Exception e) {
            //вывод исключений
            System.out.println("init error: " + e);
        }
    }
}