package com.hillel.elementary.teamleaders.examples.battleships;


import java.util.Random;
import java.util.Scanner;

public class GameStarter {

    public static void main(String[] args) {
        ShipCreator shipCreator = new ShipCreator();
        Field fieldOne = new Field(shipCreator);
        fieldOne.initializeRandomShips();
        Field fieldTwo = new Field(shipCreator);
        fieldTwo.initializeRandomShips();

        Player user = new HumanPlayer("Vasya", fieldOne);
        Player pc = new PcPlayer("PC", fieldTwo);

        Random random = new Random();
        boolean humanMovesFirst = random.nextBoolean();

        Player offender = humanMovesFirst ? user : pc;
        Player deffender = humanMovesFirst ? pc : user;
        boolean gameInProgress = true;

        while (gameInProgress) {
            int[] move = offender.move();
            switch (deffender.takeHit(move)) {
                case HIT:
                    System.out.println(deffender + " took a hit!");
                case SINKED:
                    System.out.println(deffender + "'s ship sank!");
                    if (deffender.allShipsAreSank()) gameInProgress = false;
                    System.out.println(offender + " is a Winner!");
                case MISSED:
                    Player tmp = offender;
                    offender = deffender;
                    deffender = tmp;
            }
        }
    }
}
