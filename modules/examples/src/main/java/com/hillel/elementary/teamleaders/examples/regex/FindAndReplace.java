package com.hillel.elementary.teamleaders.examples.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindAndReplace {

    public static String findAndReplace(String replaceFrom, String replaceTo, String text) {
        Pattern pattern = Pattern.compile(replaceFrom);
        Matcher matcher = pattern.matcher(text);

        return matcher.replaceAll(replaceTo);
    }

    public static String findAndAppendAfter(String appendAfter, String toAppend, String text) {
        Pattern pattern = Pattern.compile(appendAfter);
        Matcher matcher = pattern.matcher(text);

        StringBuffer stringBuffer = new StringBuffer();

        while(matcher.find()){
            String group = matcher.group();
            matcher.appendReplacement(stringBuffer, group + toAppend);
        }
        matcher.appendTail(stringBuffer);

        return stringBuffer.toString();
    }
}
