package com.hillel.elementary.teamleaders.examples.threads;

public class CalcThread extends Thread {

    private double result;

    public void run() { result = calculate(); }

    public double getResult(){
        return result;
    }

    public double calculate() {
        return 1000000 * 40000.433;
    }

}
