package com.hillel.elementary.teamleaders.examples.patterns.protorype;

public class Archer extends Warrior {
    private Type type;

    public enum Type {
        ONE, TWO
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public Copyable copy() {
        Archer copy = new Archer();
        copy.setType(this.type);
        copy.setName(this.getName());
        return copy;
    }

    @Override
    public String toString() {
        return "Archer{" +
                "type=" + type +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }
}