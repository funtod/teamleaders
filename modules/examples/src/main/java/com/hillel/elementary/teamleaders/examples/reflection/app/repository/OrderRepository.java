package com.hillel.elementary.teamleaders.examples.reflection.app.repository;

import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Order;

public interface OrderRepository {
    Order save(Order order);

    Order find(Long id);
}
