package com.hillel.elementary.teamleaders.examples.battleships;

public class Ship {
    private int size;
    private int lives;

    public Ship(int size) {
        this.size = size;
        this.lives = size;
    }

    public int hit() {
        return --lives;
    }

    public boolean isAlive() {
        return lives > 0;
    }
}
