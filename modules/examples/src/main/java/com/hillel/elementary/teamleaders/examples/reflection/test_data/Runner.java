package com.hillel.elementary.teamleaders.examples.reflection.test_data;

import com.google.gson.Gson;
import com.hillel.elementary.teamleaders.examples.reflection.test_data.data.TestData;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Runner {

    public static void main(String[] args) {
        String envName = args[0];
        initializeTests(envName);

        System.out.println(TestData.user);
        System.out.println(TestData.order);
    }

    private static void initializeTests(String envName) {

        Class<TestData> testDataClass = TestData.class;
        Field[] fields = testDataClass.getDeclaredFields();

        for (Field field : fields) {
            try {
                Path pathToJson = Paths.get(getJsonFileName(envName, field));
                Object object = new Gson().fromJson(new FileReader(pathToJson.toFile()), field.getType());
                field.set(null, object);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static String getJsonFileName(String envName, Field field) {
        String  testDataFileName = "test_data" + File.separator + envName + File.separator + field.getName() + ".json";
        return Runner.class.getClassLoader().getResource(testDataFileName).getPath();
    }
}
