package com.hillel.elementary.teamleaders.examples.battleships;

public abstract class Player {
    private String name;
    private Field field;


    public Player(String name, Field field) {
        this.name = name;
        this.field = field;
    }

    public HitStatus takeHit(int[] coord) {
        return field.takeHit(coord);
    }

    abstract int[] move();


    @Override
    public String toString() {
        return name;
    }

    public boolean allShipsAreSank() {
        return field.allShipsAreSank();
    }
}
