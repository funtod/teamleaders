package com.hillel.elementary.teamleaders.examples.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkGetter {

    public static List<String> extractLink(String text) {
        List<String> links = new ArrayList<>();

        Pattern pattern = Pattern.compile("http[s]?://(\\w?[\\w-]*\\w+\\.\\w+[\\w-]*\\w+)+(/[\\w-\\d]+)*");
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            links.add(matcher.group().trim());
        }

        return links;
    }
}
