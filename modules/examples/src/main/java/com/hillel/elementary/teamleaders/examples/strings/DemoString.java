package com.hillel.elementary.teamleaders.examples.strings;

public class DemoString {
    static int integer;

    public static String[] doDemoString(char[] arrayOfChars) {
        String str = new String(arrayOfChars); // str="Java"
        if (!str.isEmpty()) {
            integer = str.length(); // i=4

            str = str.toUpperCase(); // str="JAVA"

            String num = String.valueOf(8); // num="8"

            num = str.concat("-" + num); // num="JAVA-8"

            char ch = str.charAt(2); // ch='V'

            integer = str.lastIndexOf('A'); // i=3 (-1 если нет)

            num = num.replace("8", "SE"); // num="JAVA-SE"

            str.substring(0, 4).toLowerCase(); // java

            str = num + "-8";// str=”JAVA-SE-8”

            return str.split("-");
        } else {
            return new String[0];
        }
    }

}
