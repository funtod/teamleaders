package com.hillel.elementary.teamleaders.examples.collections;

import java.util.*;

public class SimpleArrayList implements List {
    private static final int INITIAL_CAPACITY = 16;
    private Object[] elements;
    private int size;

    public SimpleArrayList() {
        this(INITIAL_CAPACITY);
    }

    public SimpleArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    public SimpleArrayList(Collection otherCollection) {
        this.addAll(otherCollection);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (elements[i] != null && elements[i].equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elements, size);
    }

    @Override
    public boolean add(Object o) {
        ensureCapacity(size + 1);
        elements[size++] = o;
        return true;
    }

    private void ensureCapacity(int newSize) {
        if (newSize > elements.length) {
            Object[] newElements = new Object[size * 2];
            System.arraycopy(elements, 0, newElements, 0, size);
            elements = newElements;
        }
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (elements[i] != null && elements[i].equals(o)) {
                elements[i] = null;
                System.arraycopy(elements, i + 1, elements, i, size - i);
                elements[size - 1] = null;
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        if (c == null) return false;
        boolean hasChanged = false;
        for(Object obj: c) {
            boolean added = add(obj);
            if (added) hasChanged = true;
        }
        return hasChanged;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        if (c == null) return false;
        int size = c.size();
        ensureCapacity(this.size + size);
        System.arraycopy(elements, index, elements, index + size, this.size - index);

        Iterator iterator = c.iterator();
        for (int i = index; i < index + size; i++) {
            elements[i] = iterator.next();
        }
        this.size = this.size + size;
        return true;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
    }

    @Override
    public Object get(int index) {
        return elements[index];
    }

    @Override
    public Object set(int index, Object element) {
        checkIndexIsInBounds(index);
        Object oldElement = elements[index];
        elements[index] = element;
        size++;
        return oldElement;
    }

    @Override
    public void add(int index, Object element) {
        checkIndexIsInBounds(index);
        ensureCapacity(size + 1);
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = element;
        size++;
    }

    private void checkIndexIsInBounds(int index) {
        if (index >= size) throw new IllegalArgumentException("Index out of bounds");
    }

    @Override
    public Object remove(int index) {
        checkIndexIsInBounds(index);
        Object oldElement = elements[index];
        elements[index] = null;
        System.arraycopy(elements, index+1, elements, index, size - index);
        elements[size - 1] = null;
        size--;
        return oldElement;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if(elements[i] != null && elements[i].equals(o)) return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size; i >= 0; i--) {
            if(elements[i] != null && elements[i].equals(o)) return i;
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        checkIndexIsInBounds(fromIndex);
        checkIndexIsInBounds(toIndex);

        SimpleArrayList newList = new SimpleArrayList(toIndex - fromIndex);

        for (int i = fromIndex; i <= toIndex; i++) {
            newList.add(elements[i]);
        }

        return newList;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean hasChangedState = false;
        for (int i = 0; i < size; i++) {
            if (!c.contains(elements[i])) {
                remove(i);
                hasChangedState = true;
            }
        }
        return hasChangedState;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean hasChangedState = false;
        for (int i = 0; i < size; i++) {
            if (c.contains(elements[i])) {
                remove(i);
                hasChangedState = true;
            }
        }
        return hasChangedState;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (int i = 0; i < size; i++) {
            if (!c.contains(elements[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return Arrays.copyOf(elements, size);
    }
}
