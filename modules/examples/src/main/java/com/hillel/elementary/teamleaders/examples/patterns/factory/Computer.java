package com.hillel.elementary.teamleaders.examples.patterns.factory;

class Computer {
    private String display = null;
    private String systemBlock = null;
    private String manipulators = null;

    void setDisplay(String display) {
        this.display = display;
    }

    void setSystemBlock(String systemBlock) {
        this.systemBlock = systemBlock;
    }

    void setManipulators(String manipulators) {
        this.manipulators = manipulators;
    }

    public String getDisplay() {
        return display;
    }

    public String getSystemBlock() {
        return systemBlock;
    }

    public String getManipulators() {
        return manipulators;
    }
}
