package com.hillel.elementary.teamleaders.examples.threads;

public class Account {
    private double balance; //данное поле защищеное от любых несинхронных действий

    public Account(double initialDeposit) {
        balance = initialDeposit;
    }

    public synchronized double getBalance() {

        return balance;

    }

    public synchronized void deposit(double amount) {
        balance += amount;
    }
}
