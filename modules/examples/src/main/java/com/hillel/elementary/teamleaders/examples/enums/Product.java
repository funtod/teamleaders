package com.hillel.elementary.teamleaders.examples.enums;

public class Product {
    public static final int STATUS_IN_STOCK = 0;
    public static final int STATUS_BACKORDERED = 1;
    public static final int STATUS_CANCELLED = 2;

    public static final int DELIVERY_STATUS_IN_WAREHOUSE = 0;

}
