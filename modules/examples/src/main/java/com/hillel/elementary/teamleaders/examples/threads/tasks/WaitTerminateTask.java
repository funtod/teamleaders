package com.hillel.elementary.teamleaders.examples.threads.tasks;

/**
 * Как остановить поток?
 * <p>
 * Для того, чтобы прервать поток, мы можем использовать флаг
 * shouldTerminate, который должен проверяться в цикле внутри run().
 * Если флаг становится true, мы просто выходим из цикла.
 * <p>
 * Однако, тут могут быть проблемы, если от нашего потока зависят другие потоки.
 * В настоящий момент поток t2 прерывается, и программа подвисает,
 * т.к. поток t1 ждет второй поток и не может дождаться.
 * Какие есть решения проблемы?
 */
public class WaitTerminateTask {
    private final Object monitor = new Object();
    private int runningThreadNumber = 1;

    class TestThread implements Runnable {
        String threadName;

        public TestThread(String threadName) {
            this.threadName = threadName;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                System.out.println(threadName + ":" + i);
                synchronized (monitor) {
                    try {
                        while (!threadName.equals("t" + runningThreadNumber)) {
                            System.out.println("wait for thread " + "t" + runningThreadNumber);
                            monitor.wait();
                        }
                    } catch (InterruptedException e) {
                        System.out.println("Interrupted " + threadName);
                        return;
                    }
                    runningThreadNumber++;
                    if (runningThreadNumber > 2) runningThreadNumber = 1;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        System.out.println("Interrupted " + threadName);
                        return;
                    }
                    monitor.notifyAll();
                    if (Thread.currentThread().isInterrupted()) return;
                }
            }
        }
    }

    public void testThread() {
        TestThread testThread1 = new TestThread("t1");
        Thread t1 = new Thread(testThread1);
        TestThread testThread2 = new TestThread("t2");
        Thread t2 = new Thread(testThread2);
        System.out.println("Starting threads...");
        t1.start();
        t2.start();

        Thread terminator = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                t2.interrupt();
            }
        });
        terminator.start();

        System.out.println("Waiting threads to join...");
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        WaitTerminateTask tutor = new WaitTerminateTask();
        tutor.testThread();
    }
}