package com.hillel.elementary.teamleaders.examples.sockets.chat.server;

public class ServerStarter {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Enter port number");
            System.exit(0);
        }

        int port = Integer.parseInt(args[0]);
        new ChatServer().execute(port);
    }
}
