package com.hillel.elementary.teamleaders.examples.sockets.chat.server;

import java.io.*;
import java.net.*;
import java.util.*;

public class ChatServer {
    private Set<String> userNames = new HashSet<>();
    private Set<UserThread> userThreads = new HashSet<>();

    public void execute(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Chat Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New user connected");

                UserThread newUser = new UserThread(socket, this);
                userThreads.add(newUser);
                newUser.start();
            }
        } catch (IOException ex) {
            System.out.println("Error in the server: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Delivers a message from one user to others (broadcasting)
     */
    void broadcast(String message, UserThread excludeUser) {
        userThreads.stream()
                .filter( user -> user != excludeUser)
                .forEach(user -> user.sendMessage(message));
    }

    /**
     * Stores username of the newly connected client.
     */
    void addUserName(String userName) {
        userNames.add(userName);
    }

    /**
     * When a client is disconnected, removes the associated username and UserThread
     */
    void removeUser(String userName, UserThread aUser) {
        if (userNames.remove(userName)) {
            userThreads.remove(aUser);
            String userLeftMessage = "The user " + userName + " quit";
            System.out.println(userLeftMessage);
            broadcast(userLeftMessage, null);
        }
    }

    Set<String> getUserNames() {
        return this.userNames;
    }

    boolean hasUsers() {
        return !this.userNames.isEmpty();
    }

}
