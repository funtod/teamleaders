package com.hillel.elementary.teamleaders.examples.reflection.test_data.data;

import java.math.BigDecimal;
import java.util.List;

public class Order {
    public BigDecimal grandTotal;
    public BigDecimal netTotal;
    public BigDecimal tax;
    public BigDecimal discounts;
    public List<Product> productList;

    @Override
    public String toString() {
        return "Order{" +
                "grandTotal=" + grandTotal +
                ", netTotal=" + netTotal +
                ", tax=" + tax +
                ", discounts=" + discounts +
                ", productList=" + productList +
                '}';
    }
}
