package com.hillel.elementary.teamleaders.examples.arrays;

public class BinarySearch {

    public static int findIndex(int toFind, int[] array) {

        int left = 0;
        int right = array.length;

        while (left < right) {
            int middle = (left + right) / 2;

            if (array[middle] < toFind) {
                left = middle + 1;
            } else if (array[middle] > toFind) {
                right = middle;
            } else {
                return middle;
            }
        }

        return -1;
    }

}
