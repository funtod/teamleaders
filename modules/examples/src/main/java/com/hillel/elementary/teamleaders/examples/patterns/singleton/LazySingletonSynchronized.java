package com.hillel.elementary.teamleaders.examples.patterns.singleton;

public class LazySingletonSynchronized {

    private static volatile LazySingletonSynchronized instance;

    private LazySingletonSynchronized() {
    }

    public static LazySingletonSynchronized getInstance() {
        if (instance == null) {
            synchronized (LazySingletonSynchronized.class) {
                if (instance == null) {
                    instance = new LazySingletonSynchronized();
                }
            }
        }
        return instance;
    }

}
