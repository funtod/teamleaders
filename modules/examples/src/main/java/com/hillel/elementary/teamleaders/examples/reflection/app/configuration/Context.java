package com.hillel.elementary.teamleaders.examples.reflection.app.configuration;

public interface Context {

    <T> T getBean(String beanName) throws Exception;

}
