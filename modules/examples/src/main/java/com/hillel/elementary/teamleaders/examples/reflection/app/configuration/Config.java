package com.hillel.elementary.teamleaders.examples.reflection.app.configuration;

public interface Config {

    Class<?> getImplementation(String name);

}
