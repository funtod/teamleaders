package com.hillel.elementary.teamleaders.examples.reflection.test_data.data;


public class User {
    public String name;
    public Long id;
    public Boolean isPremiumCustomer;
    public String phone;
    public Address billingAddress;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", isPremiumCustomer=" + isPremiumCustomer +
                ", phone='" + phone + '\'' +
                ", billingAddress=" + billingAddress +
                '}';
    }
}
