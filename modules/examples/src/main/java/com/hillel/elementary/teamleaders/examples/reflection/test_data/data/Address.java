package com.hillel.elementary.teamleaders.examples.reflection.test_data.data;

public class Address {
    public String street;
    public String postCode;
    public String countryCode;
    public String housNumber;

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", postCode='" + postCode + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", housNumber='" + housNumber + '\'' +
                '}';
    }
}
