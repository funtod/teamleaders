package com.hillel.elementary.teamleaders.examples.enums;

public enum Color {
    RED("#dd2c2c"),
    GREEN("#23b75f"),
    BLUE("#4290ae"),
    WHITE("#fafafa"),
    BLACK("#000000"),
    GRAY("#999999"),
    YELLOW("#ffc200"); // точка с запятой после последнего

    public final String hex;

    Color(String hex) {
        this.hex = hex;
    }

    public static Color fromHex(String hex) {
        for (Color color : values()) {
            if (color.hex.equalsIgnoreCase(hex.trim())) {
                return color;
            }
        }
        throw new IllegalArgumentException(String.format("Color not found for hex: %s", hex));
    }

}
