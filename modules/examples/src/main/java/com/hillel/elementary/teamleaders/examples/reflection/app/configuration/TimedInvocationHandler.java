package com.hillel.elementary.teamleaders.examples.reflection.app.configuration;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TimedInvocationHandler implements InvocationHandler {
    private Object target;
    private Class<?> type;

    public TimedInvocationHandler(Object target) {
        this.target = target;
        this.type = target.getClass();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Timed timedAnnotation = getRealMethod(method).getAnnotation(Timed.class);
        if (timedAnnotation != null && timedAnnotation.isOn()) {
            long startTime = System.currentTimeMillis();
            try {
                return method.invoke(target, args);
            } finally {
                long endTime = System.currentTimeMillis();
                System.out.printf("Method %s of bean %s took %sms to complete\n", method.getName(), type.getSimpleName(), endTime - startTime);
            }
        }
        return method.invoke(target, args);
    }

    private Method getRealMethod(Method method) throws NoSuchMethodException {
        return type.getMethod(method.getName(), method.getParameterTypes());
    }
}
