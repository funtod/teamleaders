package com.hillel.elementary.teamleaders.examples.reflection.app.services;

import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Customer;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Order;

public interface OrderService {

    Order placeNewOrder(Customer customer, Long... pizzaID);

}
