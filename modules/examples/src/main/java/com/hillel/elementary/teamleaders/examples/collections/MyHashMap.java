package com.hillel.elementary.teamleaders.examples.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class MyHashMap<K, V> implements Map<K, V> {
    private static final int INITIAL_CAPASITY = 16;

    private int size;

    private Node<K, V>[] table;


    public MyHashMap() {
        table = new Node[INITIAL_CAPASITY];
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        int hash = getHash(key);
        int bucket = getBucket(hash);

        Node<K, V> node = table[bucket];

        while (node != null) {
            if (node.hash == hash && node.key.equals(key)) {
                return node.value;
            }
            node = node.next;
        }


        return null;
    }

    @Override
    public V put(K key, V value) {
        int hash = getHash(key);

        int bucket = getBucket(hash);

        Node<K, V> node = table[bucket];

        if (node == null) {
            table[bucket] = new Node<>(hash, key, value, null);
            size++;
        } else {
            while (node != null) {
                if (node.hash == hash && node.key.equals(key)) {
                    V oldValue = node.value;
                    node.value = value;

                    return oldValue;
                }
                if (node.next == null) {
                    node.next = new Node<>(hash, key, value, null);
                    size++;
                    return null;
                }
                node = node.next;
            }

        }

        return null;
    }


    @Override
    public V remove(Object key) {
        int hash = getHash(key);
        int bucket = getBucket(hash);

        Node<K, V> node = table[bucket];

        if (keysAreEqual(node, hash, key)) {
            if (node.next != null) {
                table[bucket] = node.next;
            } else {
                table[bucket] = null;
            }
            size--;
            return node.value;
        }

        Node<K, V> prev = null;
        while (node != null) {
            if (keysAreEqual(node, hash, key)) {
                prev.next = node.next;
                size--;
                return node.value;
            }

            prev = node;
            node = node.next;
        }

        return null;
    }

    private boolean keysAreEqual(Node<K, V> node, int hash, Object key) {
        return node != null && (node.hash == hash && node.key.equals(key));
    }


    private int getHash(Object key) {
        return key == null ? 0 : key.hashCode();
    }

    private int getBucket(int hash) {
        return (table.length - 1) & hash;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private static class Node<K, V> {
        private int hash;
        private K key;
        private V value;
        private Node<K, V> next;

        private Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
