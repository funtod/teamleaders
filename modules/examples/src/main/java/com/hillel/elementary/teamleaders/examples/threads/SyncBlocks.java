package com.hillel.elementary.teamleaders.examples.threads;

public class SyncBlocks {

    public static void abs(int[] values) {
        synchronized (values) { //доступ к массиву values блокируется со стороны других потоков
            for (int i = 0; i < values.length; i++) {
                if (values[i] < 0) values[i] = -values[i];
            }
        }
    }
}
