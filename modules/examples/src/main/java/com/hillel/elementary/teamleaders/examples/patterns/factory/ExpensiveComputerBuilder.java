package com.hillel.elementary.teamleaders.examples.patterns.factory;

class ExpensiveComputerBuilder extends ComputerBuilder {

    public void buildSystemBlock() {
        computer.setSystemBlock("CORSAIR");
    }

    public void buildDisplay() {
        computer.setDisplay("Samsung");
    }

    public void buildManipulators() {
        computer.setManipulators("mouse+keyboard+headset");
    }
}
