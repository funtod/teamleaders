package com.hillel.elementary.teamleaders.examples.java8.tasks;

import java.util.List;

public class FindDistinctTask {

    // Вернуть список с уникальными задачами
    public static List<Task> allDistinctTasks(List<Task> tasks) {
        throw new UnsupportedOperationException();
    }

    // Найти 5 первых (по дате создания) задач по чтению
    public static List<String> top5ReadingTaskNames(List<Task> tasks) {
        throw new UnsupportedOperationException();
    }

    // Посчитать количество задач указанного типа
    public static Integer countAllTasksByType(List<Task> tasks, TaskType type) {
        throw new UnsupportedOperationException();
    }

    // Вернуть все уникальные теги задач
    public static List<String> getAllDistinctTags(List<Task> tasks) {
        throw new UnsupportedOperationException();
    }

    // Проверить у всех ли задач по чтению есть тег "Books"
    public static Boolean isAllReadingTasksWithTagBooks(List<Task> tasks) {
        throw new UnsupportedOperationException();
    }

    //Сконкатенировать все названия задач в одну надпись через "***"
    public static String joinAllTaskTitles(List<Task> tasks) {
        throw new UnsupportedOperationException();
    }
}
