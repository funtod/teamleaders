package com.hillel.elementary.teamleaders.examples.sockets.chat.server;

import java.io.*;
import java.net.*;

public class UserThread extends Thread {
    private Socket socket;
    private ChatServer server;
    private String userName;
    private PrintWriter writer;

    UserThread(Socket socket, ChatServer server) {
        this.socket = socket;
        this.server = server;
    }

    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             OutputStream output = socket.getOutputStream();
             PrintWriter writer = new PrintWriter(output, true);) {
            this.writer = writer;
            printUsers();

            userName = reader.readLine();
            server.addUserName(userName);

            String serverMessage = "New user connected: " + userName;
            server.broadcast(serverMessage, this);

            String clientMessage = reader.readLine();
            serverMessage = "[" + userName + "]: " + clientMessage;
            server.broadcast(serverMessage, this);

            while (!clientMessage.equals("bye")) {
                clientMessage = reader.readLine();
                serverMessage = "[" + userName + "]: " + clientMessage;
                server.broadcast(serverMessage, this);
            }

            serverMessage = userName + " has quitted.";
            server.broadcast(serverMessage, this);

        } catch (IOException e) {
            System.out.println("Error in UserThread: " + e.getMessage());
        } finally {
            server.removeUser(userName, this);
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Error in UserThread: " + e.getMessage());
            }
        }
    }

    /**
     * Sends a list of online users to the newly connected user.
     */
    private void printUsers() {
        if (server.hasUsers()) {
            writer.println("Connected users: " + server.getUserNames());
        } else {
            writer.println("No other users connected");
        }
    }

    /**
     * Sends a message to the client.
     */
    void sendMessage(String message) {
        writer.println(message);
    }
}