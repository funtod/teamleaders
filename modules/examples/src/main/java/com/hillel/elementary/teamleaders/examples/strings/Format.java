package com.hillel.elementary.teamleaders.examples.strings;

public class Format {

    public static String format(float floatVar, int intVar, String stringVar) {
        return String.format("The value of the float variable is %.2f, while " +
                        "the value of the integer variable is %d, " +
                        "and the string is %s",
                floatVar, intVar, stringVar);
    }
}
