package com.hillel.elementary.teamleaders.examples.patterns.protorype;

public interface Copyable {
    Copyable copy();
}
