package com.hillel.elementary.teamleaders.examples.primitives;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUsage {


    public static void main(String[] args) {
        BigDecimal ten = new BigDecimal("10.000");
        BigDecimal notTen = new BigDecimal("11.199");

        BigDecimal result = ten.divide(notTen, RoundingMode.HALF_UP);

        System.out.println(result);

    }
}
