package com.hillel.elementary.teamleaders.examples.patterns.factory_method;

import com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors.Archer;
import com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors.Horseman;
import com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors.Infantryman;
import com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors.Warrior;

public class FactoryMethodExample {

    public static void main(String[] args) {
        Warrior archer = new Archer();
        Warrior horseman = new Horseman();
        Warrior infantryman = new Infantryman();


        System.out.println(archer.stats());
        System.out.println(horseman.stats());
        System.out.println(infantryman.stats());
    }
}
