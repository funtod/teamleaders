package com.hillel.elementary.teamleaders.examples.jdbc.dao;

public abstract class DAOFactory {

// Список DAO типов поддерживаемых фабрикой

    enum FactoryType {
        CLOUDSCAPE,
        ORACLE,
        SYBASE
    }

// Методы для создания DAO

    public abstract CustomerDAO getCustomerDAO();

//    public abstract AccountDAO getAccountDAO();

//    public abstract OrderDAO getOrderDAO();

    public static DAOFactory getDAOFactory(FactoryType whichFactory) {

        switch (whichFactory) {

            case CLOUDSCAPE:
                return new CloudscapeDAOFactory();

//            case ORACLE:
//                return new OracleDAOFactory();
//
//            case SYBASE:
//                return new SybaseDAOFactory();

            default:
                throw new IllegalArgumentException("Unkown DAOFactory type: "  + whichFactory);

        }
    }
}