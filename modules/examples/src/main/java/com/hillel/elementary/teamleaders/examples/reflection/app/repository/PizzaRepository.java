package com.hillel.elementary.teamleaders.examples.reflection.app.repository;

import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Pizza;

public interface PizzaRepository {
    Pizza find(Long id);
}
