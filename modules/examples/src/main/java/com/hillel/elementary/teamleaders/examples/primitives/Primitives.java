package com.hillel.elementary.teamleaders.examples.primitives;

public class Primitives {
    int a;


    public static void main(String[] args) {
        Primitives primitives = new Primitives();
        primitives.a = 10;

        Primitives second = primitives;

        primitives.a = 20;

        System.out.println(second.a);

        int number = 10;
        int secondNumber = number;

        number = 20;
        System.out.println(secondNumber);

    }


}
