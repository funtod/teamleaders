package com.hillel.elementary.teamleaders.examples.classes.nested_static_class;


public class InnerClassChild extends BankAccount.Permission {

    public InnerClassChild(String permissionType) {
        super(permissionType);
    }
}
