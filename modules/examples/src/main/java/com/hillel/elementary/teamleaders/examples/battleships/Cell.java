package com.hillel.elementary.teamleaders.examples.battleships;

public class Cell {
    private Ship ship;


    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
