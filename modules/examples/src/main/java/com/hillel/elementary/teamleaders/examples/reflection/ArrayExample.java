package com.hillel.elementary.teamleaders.examples.reflection;

import java.lang.reflect.Array;

public class ArrayExample {
    public static void main(String[] args) {
        byte[] ba = (byte[]) Array.newInstance(byte.class,13);

        int[] dims = {4, 4};
        double[][] matrix =(double[][]) Array.newInstance(double.class, dims);
    }
}
