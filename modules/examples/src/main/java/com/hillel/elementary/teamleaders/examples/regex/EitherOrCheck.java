package com.hillel.elementary.teamleaders.examples.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class EitherOrCheck {

    public static List<String> matchJimOrJoe(String text) {
        List<String> matches = new ArrayList<>();

        Pattern pattern = Pattern.compile(".*(jim|joe).*", CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            matches.add(matcher.group(1).toUpperCase());
        }
        return matches;
    }


    public static List<String> matchNotAJimJoeLine(String text) {
        List<String> matches = new ArrayList<>();

        Pattern pattern = Pattern.compile("(^|\r|\n)(?!.*(jim|joe)).+", CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            matches.add(matcher.group(0).trim());
        }
        return matches;
    }
}
