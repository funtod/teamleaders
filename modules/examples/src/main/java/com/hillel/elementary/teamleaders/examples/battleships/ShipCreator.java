package com.hillel.elementary.teamleaders.examples.battleships;

import java.util.Random;

public class ShipCreator {
    private Random random = new Random();

    public void createShip(int[][] field, Cell[][] cells, int shipSize) {
        int[][] shipCoords = generateNext(field, shipSize);
        while (shipCoords == null) {
            shipCoords = generateNext(field, shipSize);
        }

        Ship ship = new Ship(shipSize);

        for (int i = 0; i < shipSize; i++) {
            field[shipCoords[i][0]][shipCoords[i][1]] = 1;
            cells[shipCoords[i][0]][shipCoords[i][1]].setShip(ship);
        }
    }

    private int[][] generateNext(int[][] field, int shipSize) {
        int[][] shipCoords = null;
        int startingX = random.nextInt(9);
        int startingY = random.nextInt(9);

        if (field[startingX][startingY] == 0) {
            shipCoords = fillShipCoords(shipSize, startingX, startingY);
            if (cannotBePlaced(field, shipCoords)) shipCoords = null;
        }

        return shipCoords;
    }

    private int[][] fillShipCoords(int shipSize, int startingX, int startingY) {
        int[][] shipCoords;
        shipCoords = new int[shipSize][2];
        shipCoords[0][0] = startingX;
        shipCoords[0][1] = startingY;

        if (startingX - shipSize >= 0) {
            for (int i = 1; i < shipSize; i++) {
                shipCoords[i][0] = startingX - i;
                shipCoords[i][1] = startingY;
            }
        } else if (startingX + shipSize < 10) {
            for (int i = 1; i < shipSize; i++) {
                shipCoords[i][0] = startingX + i;
                shipCoords[i][1] = startingY;
            }
        } else if (startingY - shipSize >= 0) {
            for (int i = 1; i < shipSize; i++) {
                shipCoords[i][0] = startingX;
                shipCoords[i][1] = startingY - i;
            }
        } else if (startingY + shipSize < 10) {
            for (int i = 1; i < shipSize; i++) {
                shipCoords[i][0] = startingX;
                shipCoords[i][1] = startingY + i;
            }
        }
        return shipCoords;
    }

    private boolean cannotBePlaced(int[][] field, int[][] shipCoords) {
        for (int[] shipCoord : shipCoords) {
            int x = shipCoord[0];
            int y = shipCoord[1];

            if (field[x][y] != 0) {
                return true;
            }
            if (x > 0 && field[x - 1][y] != 0) {
                return true;
            }
            if (x < 9 && field[x + 1][y] != 0) {
                return true;
            }
            if (y > 0 && field[x][y - 1] != 0) {
                return true;
            }
            if (y < 9 && field[x][y + 1] != 0) {
                return true;
            }
        }
        return false;
    }
}
