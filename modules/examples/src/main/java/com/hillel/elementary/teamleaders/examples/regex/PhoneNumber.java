package com.hillel.elementary.teamleaders.examples.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumber {

    public static List<String> getPhoneNumbers(String text) {
        List<String> phones = new ArrayList<>();

        Pattern pattern = Pattern.compile("(\\+\\d{2})?\\s*(\\(\\d{3}\\))?\\s*((\\d[- ]?)*\\d)+");
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            phones.add(matcher.group().trim());
        }
        return phones;
    }
}
