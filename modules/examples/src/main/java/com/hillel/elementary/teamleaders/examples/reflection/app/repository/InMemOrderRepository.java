package com.hillel.elementary.teamleaders.examples.reflection.app.repository;

import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Component;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Order;

import java.util.HashMap;
import java.util.Map;

@Component("orderRepository")
public class InMemOrderRepository implements OrderRepository  {

    private final Map<Long, Order> orders = new HashMap<>();
    private Long counter = 0L;

    @Override
    public Order save(Order order) {
        orders.put(counter++, order);
        order.setId(counter);
        return order;
    }

    @Override
    public Order find(Long id) {
        return orders.get(id);
    }

}
