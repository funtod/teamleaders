package com.hillel.elementary.teamleaders.examples.sockets.chat.client;

public class ClientStarter {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Enter server hostname and port number");
        }

        String hostname = args[0];
        int port = Integer.parseInt(args[1]);

        new ChatClient(hostname).execute(port);
    }
}
