package com.hillel.elementary.teamleaders.examples.sockets;

import java.io.*;

import java.net.*;

class ServerSocketExample extends Thread {

    public static void main(String args[]) {
        try {
            int i = 0; // счётчик подключений
            // привинтить сокет на localhost, порт 3128
            ServerSocket server = new ServerSocket(3128, 0, InetAddress.getByName("localhost"));
            System.out.println("server is started");
            // слушаем порт
            while (true) {
                // ждём нового подключения, после чего запускаем обработку клиента
                // в новый вычислительный поток и увеличиваем счётчик на единицу
                new SampleServer(i++, server.accept());
            }
        } catch (Exception e) {
            System.out.println("init error: " + e);
        }
    }

    static class SampleServer extends Thread {
        private Socket socket;
        private int num;

        SampleServer(int num, Socket socket) {
            // копируем данные
            this.num = num;
            this.socket = socket;
            // и запускаем новый вычислительный поток
            setDaemon(true);
            start();
        }

        public void run() {
            try (// из сокета клиента берём поток входящих данных
                 InputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
                 // и оттуда же - поток данных от сервера к клиенту
                 OutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream())) {
                // буфер данных в 64 килобайта
                byte buf[] = new byte[64 * 1024];
                // читаем 64кб от клиента, результат - кол-во реально принятых данных
                int numberOfBytes = bufferedInputStream.read(buf);

                // создаём строку, содержащую полученную от клиента информацию
                String data = new String(buf, 0, numberOfBytes);
                data = "" + num + ": " + " " + data;
                // возвращаем назад строку с номером соединения:
                bufferedOutputStream.write(data.getBytes());
                System.out.println("Server: data sent: " + data);
            } catch (Exception e) {
                System.out.println("init error: " + e);
            } finally {
                try {
                    socket.close(); // завершаем соединение
                } catch (IOException e) {
                    System.out.println("init error: " + e);
                }
            }
        }
    }
}
