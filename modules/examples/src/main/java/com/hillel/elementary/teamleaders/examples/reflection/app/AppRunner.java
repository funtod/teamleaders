package com.hillel.elementary.teamleaders.examples.reflection.app;

import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.ApplicationContext;
import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.Context;
import com.hillel.elementary.teamleaders.examples.reflection.app.configuration.JavaConfig;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Customer;
import com.hillel.elementary.teamleaders.examples.reflection.app.domain.Order;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.InMemOrderRepository;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.InMemPizzaRepository;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.OrderRepository;
import com.hillel.elementary.teamleaders.examples.reflection.app.repository.PizzaRepository;
import com.hillel.elementary.teamleaders.examples.reflection.app.services.OrderService;
import com.hillel.elementary.teamleaders.examples.reflection.app.services.PizzaService;
import com.hillel.elementary.teamleaders.examples.reflection.app.services.SimpleOrderService;
import com.hillel.elementary.teamleaders.examples.reflection.app.services.SimplePizzaService;

public class AppRunner {
    public static void main(String[] args)  throws Exception {

        System.out.println("Pizza Srervice");

        Customer customer = null;
//        Order order;
//        PizzaRepository pizzaRepository = new InMemPizzaRepository();
//        PizzaService pizzaService = new SimplePizzaService(pizzaRepository);
//        OrderRepository orderRepository = new InMemOrderRepository();
//        OrderService orderService = new SimpleOrderService(orderRepository, pizzaService);
//        order = orderService.placeNewOrder(customer, 1L, 2L, 3L);
//
//        System.out.println(order);

        Context context = new ApplicationContext(new JavaConfig());

        PizzaRepository pizzaRepository = context.getBean("pizzaRepository");
        System.out.println("Found " + pizzaRepository.find(1L));

        OrderService orderService = context.getBean("orderService");
        Order order = orderService.placeNewOrder(customer, 1L, 2L, 3L);
        System.out.println(order);
    }
}
