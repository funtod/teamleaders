package com.hillel.elementary.teamleaders.examples.collections;

import java.util.*;

public class SimplePriorityQueue<E> implements Queue<E> {

    private static final int INITIAL_SIZE = 16;
    private int size;
    private E[] elements;
    private Comparator<E> comparator;

    public SimplePriorityQueue() {
        this(INITIAL_SIZE);
    }


    public SimplePriorityQueue(int initialSize) {
        elements = (E[]) new Object[initialSize];
    }

    public SimplePriorityQueue(Comparator<E> comparator) {
        this();
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        if (e == null) throw new IllegalArgumentException("nulls not allowed");
        checkSize(size + 1);

        if (size == 0) {
            elements[0] = e;
        } else {
            int index = shiftUp(e);
            elements[index] = e;
        }

        size++;

        return true;
    }

    private int shiftUp(E e) {
        int index;

        if (comparator != null) {
            index = shiftUpWithComparator(e);
        } else {
            index = shiftUpWithComparable(e);
        }
        return index;
    }

    private int shiftUpWithComparable(E e) {
        int index = size;
        for (int parentIndex = ((index - 1) / 2); index > 0; parentIndex = ((index - 1) / 2)) {
            if (((Comparable<E>) e).compareTo(elements[parentIndex]) > 0) {
                elements[index] = elements[parentIndex];
                index = parentIndex;
            } else {
                break;
            }
        }
        return index;
    }

    private int shiftUpWithComparator(E e) {
        int index = size;
        for (int parentIndex = (index - 1) / 2; parentIndex >= 0; ) {
            if (comparator.compare(e, elements[parentIndex]) > 0) {
                elements[index] = elements[parentIndex];
                index = parentIndex;
            } else {
                break;
            }

        }
        return index;
    }


    private void checkSize(int newSize) {
        if (newSize > elements.length) {
            elements = Arrays.copyOf(elements, size * 2);
        }
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public E remove() {
        return null;
    }

    @Override
    public E poll() {
        if (size == 0) return null;

        E e = elements[0];

        elements[0] = elements[size - 1];
        elements[size - 1] = null;

        siftDown();

        size--;
        return e;
    }

    private void siftDown() {
        if (comparator != null) {
            siftDownWithComparator();
        } else {
            siftDownWithComparable();
        }
    }

    private void siftDownWithComparator() {
        int index = 0;
        E lastElement = elements[index];

        for(;;) {
            int childIndex = (index * 2) + 1;
            int rightChildIndex = (index * 2) + 2;

            E child = elements[childIndex];

            if (elements[rightChildIndex] != null
                    && comparator.compare(elements[rightChildIndex], child) > 0) {
                child = elements[rightChildIndex];
                childIndex = rightChildIndex;
            }

            if (comparator.compare(elements[index], child) < 0) {
                elements[index] = child;
                index = childIndex;
            } else {
                break;
            }
        }

        elements[index] = lastElement;

    }

    private void siftDownWithComparable() {
        int index = 0;
        E lastElement = elements[index];

        for(; index < (size / 2);) {
            int childIndex = (index * 2) + 1;
            int rightChildIndex = (index * 2) + 2;

            E child = elements[childIndex];

            if (child == null) break;

            if (elements[rightChildIndex] != null
                    && ((Comparable<E>)elements[rightChildIndex]).compareTo(child) > 0) {
                child = elements[rightChildIndex];
                childIndex = rightChildIndex;
            }

            if (((Comparable<E>)elements[index]).compareTo(child) < 0) {
                elements[index] = child;
                index = childIndex;
            } else {
                break;
            }
        }

        elements[index] = lastElement;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }
}
