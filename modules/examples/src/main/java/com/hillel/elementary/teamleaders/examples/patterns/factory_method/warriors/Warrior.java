package com.hillel.elementary.teamleaders.examples.patterns.factory_method.warriors;

public abstract class Warrior {

    public String stats() {
        return "This is a general part + " + info();
    }
    protected abstract String info();
}
