package com.hillel.elementary.teamleaders.examples.classes.abstract_class;

public abstract class AbstractClass {

    public String returnSomething() {
        return "something";
    }

    public abstract String abstractMethod(); // абстрактный метод
}
