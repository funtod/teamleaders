package com.hillel.elementary.teamleaders.examples.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class IteratorExample {

    public static void main(String[] args) {
        ArrayList<Double> doubles = new ArrayList<>(7);
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            doubles.add(random.nextGaussian());
        }

        for (Double d : doubles) {
            System.out.printf("%.2f ", d);
        }

        int positiveNum = 0;
        int initialSize = doubles.size();
        Iterator<Double> iterator = doubles.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() > 0) {
                positiveNum++;
            } else {
                iterator.remove();
            }
        }

        System.out.printf("%n Положительных: % d", positiveNum);
        System.out.printf("%n Неположительных: % d", initialSize - positiveNum);
        System.out.println("\n Положительная коллекция");

        for (Double d : doubles) {
            System.out.printf("%.2f ", d);
        }

    }
}
