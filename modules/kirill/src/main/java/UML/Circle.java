package UML;

public class Circle {
    private double radius;
    private String color;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {

        this.radius = radius;
        this.color = color;
    }

    public Circle() {

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getCircumference() {
        double perimeter = 2 * radius * 3.14;
        return perimeter;
    }

    public double getArea() {
        double area = 3.14 + Math.pow(radius, 2);
        return area;
    }


}
