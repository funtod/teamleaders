package UML;

public class CircleWithCoordinates extends Circle {
    private Point center;

    public CircleWithCoordinates(Point center) {
        this.center = center;
    }

    public CircleWithCoordinates(Point center, double radius) {
        super(radius);
        this.center = center;
    }

    public CircleWithCoordinates(Point center, double radius, String color) {
        super(radius, color);
        this.center = center;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double distance(Point other) {
        return Math.sqrt(Math.pow((this.center.x - other.x), 2) + Math.pow((this.center.y - other.y), 2));
    }

}
