package com.hillel.elementary.teamleaders.kirill.Task2;

import java.util.LinkedList;
import java.util.List;

public class OperationsWithTrain {

    public static List<Train> trainsWithSameDestination(Train[] trains, String destination) {
        List<Train> trainList = new LinkedList<Train>();
        for (int i = 0; i < trains.length; i++) {
            if (trains[i].getDestination().equals(destination)) {
                trainList.add(trains[i]);
            }
        }
        return trainList;

    }

    public static List<Train> trainsWithSameDestinationAndAfterTime(Train[] trains, String destination, double time) {
        List<Train> trainList = new LinkedList<Train>();
        for (int i = 0; i < trains.length; i++) {
            if (trains[i].getDestination().equals(destination) && trains[i].getTime() > time) {
                trainList.add(trains[i]);
            }
        }
        return trainList;
    }

    public static List<Train> trainsWithSameDestinationAndNumberOfPlaces(Train[] trains, String destination) {
        List<Train> trainList = new LinkedList<Train>();
        for (int i = 0; i < trains.length; i++) {
            for (int j = 1; j < trains.length; j++)
                if (trains[i].getDestination().equals(destination) && trains[i].getNumberOfPlaces().equals(trains[j].getNumberOfPlaces()) && trains[j].getDestination().equals(destination)) {
                    trainList.add(trains[i]);
                }
        }
        return trainList;
    }
}
