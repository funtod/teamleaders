package com.hillel.elementary.teamleaders.task1;

public class Polindrom {
    public static boolean isPalindrome(int inputInt) {
        int palidrome = inputInt;
        int reverse = 0;

        while (palidrome != 0) {
            int reminder = palidrome % 10;
            reverse = reverse * 10 + reminder;
            palidrome = palidrome / 10;
        }
        return inputInt == reverse;
    }
}