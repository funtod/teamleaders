package com.hillel.elementary.teamleaders.kirill.Task2;

import java.util.Objects;

public class Train {
    private String destination;
    private int trainSerialNumber;
    private double time;
    private int numberOfPlaces;

    public Train(String destination, int trainSerialNumber, double time, int numberOfPlaces) {
        this.destination = destination;
        this.trainSerialNumber = trainSerialNumber;
        this.time = time;
        this.numberOfPlaces = numberOfPlaces;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTrainSerialNumber() {
        return trainSerialNumber;
    }

    public void setTrainSerialNumber(int trainSerialNumber) {
        this.trainSerialNumber = trainSerialNumber;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Integer getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return trainSerialNumber == train.trainSerialNumber &&
                Double.compare(train.time, time) == 0 &&
                numberOfPlaces == train.numberOfPlaces &&
                Objects.equals(destination, train.destination);
    }

    @Override
    public int hashCode() {

        return Objects.hash(destination, trainSerialNumber, time, numberOfPlaces);
    }
}
