package com.hillel.elementary.teamleaders.kirill.Task2.Test;

import com.hillel.elementary.teamleaders.kirill.Task2.OperationsWithTrain;
import com.hillel.elementary.teamleaders.kirill.Task2.Train;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationWithTrainTest {

    @Test
    void shouldReturnTrueIfDestinationSame() {
        Train[] trains = new Train[3];

        Train one = new Train("a", 123, 22.30, 40);
        Train two = new Train("b", 456, 21.00, 50);
        Train three = new Train("a", 789, 20.30, 60);

        trains[0] = one;
        trains[1] = two;
        trains[2] = three;

        List<Train> expected = new LinkedList<Train>();
        expected.add(one);
        expected.add(three);


        List<Train> result = OperationsWithTrain.trainsWithSameDestination(trains, "a");


        assertEquals(expected, result);
    }

    @Test
    void shouldReturnTrueIfDestinationSameAndTimeMoreThanTemp() {
        Train[] trains = new Train[3];

        Train one = new Train("a", 123, 22.30, 40);
        Train two = new Train("b", 456, 21.00, 50);
        Train three = new Train("a", 789, 20.30, 60);

        trains[0] = one;
        trains[1] = two;
        trains[2] = three;

        List<Train> expected = new LinkedList<Train>();
        expected.add(one);
        expected.add(three);

        double tempTime = 20.29;


        List<Train> result = OperationsWithTrain.trainsWithSameDestinationAndAfterTime(trains, "a", tempTime);


        assertEquals(expected, result);
    }

    @Test
    void shouldReturnTrueIfDestinationAndNumberOfPlacesSame() {
        Train[] trains = new Train[3];

        Train one = new Train("a", 123, 22.30, 40);
        Train two = new Train("b", 456, 21.00, 50);
        Train three = new Train("a", 789, 20.30, 40);

        trains[0] = one;
        trains[1] = two;
        trains[2] = three;

        List<Train> expected = new LinkedList<Train>();
        expected.add(one);
        expected.add(three);


        List<Train> result = OperationsWithTrain.trainsWithSameDestinationAndNumberOfPlaces(trains, "a");


        assertEquals(expected, result);
    }


}
