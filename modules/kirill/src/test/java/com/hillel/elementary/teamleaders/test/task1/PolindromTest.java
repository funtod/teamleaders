package com.hillel.elementary.teamleaders.test.task1;


import com.hillel.elementary.teamleaders.task1.Polindrom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PolindromTest {
    @Test
    void shouldReturnTrueIfPolyndrome() {
        int input = 505;

        boolean result = Polindrom.isPalindrome(input);
        assertEquals(true, result);
    }

    @Test
    void shouldReturnFalseIfNotPolyndrome() {
        int input = 5025;

        boolean result = Polindrom.isPalindrome(input);
        assertEquals(false, result);
    }
}